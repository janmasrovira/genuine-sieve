% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice.

\documentclass[xcolor=x11names]{beamer}

\usepackage{colortbl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{url}
\usepackage{booktabs}
\usepackage{float}
\usepackage{minted}
\usepackage{nth}
\usetheme{Madrid}

\graphicspath{ {figures/} }

% red text
\newcommand{\imp}[1]{\textcolor{red}{#1}}
\newcommand{\sieve}[0]{Sieve of Eratosthenes}



\title{The genuine \sieve}

% A subtitle is optional and this may be deleted
\subtitle{Lazy, functional and efficient}

\author{Jan Mas Rovira}
% - Give the names in the same order as the appear in the paper.
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute[FIB --- UPC] % (optional, but mostly needed)
{
  Factultat d'Informàtica de Barcelona,\\
  Universitat Politècnica de Catalunya
  \and
}
% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.

\date{TMIRI, 2015}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

\subject{Theoretical Computer Science}
% This is only inserted into the PDF information catalog. Can be left
% out.

% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
% \AtBeginSubsection[]
% {
%   \begin{frame}<beamer>{Outline}
%     \tableofcontents[currentsection,currentsubsection]
%   \end{frame}
% }

% Let's get started
\begin{document}



\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents
  % You might wish to add the option [pausesections]
\end{frame}

% Section and subsections will appear in the presentation overview
% and table of contents.
\section{Introduction}

\begin{frame}{Original Paper}
  %\frametitle<presentation>{Original Paper}
  The paper was published by Melissa O'Neill in 2009 in the
  Journal of Functional Programming.

  \begin{thebibliography}{10}

  \beamertemplatearticlebibitems

  \bibitem{Someone2000}
    Melissa O'Neill.
    \newblock {The genuine \sieve.}
    \newblock {2009, \em Journal of Functional Programming}, vol. 19, pag. 95--106.
  \end{thebibliography}
\end{frame}

\begin{frame}{Introduction}
\begin{block}{Prime definition}
  A \imp{prime} is a natural number greater than 1 that has no
  positive divisors other than 1 and itself.
\end{block}
~\\
The \imp{\sieve{}} is an algorithm for finding all the primes
up to a certain limit.

 \begin{figure}[h]
    \centering
    \includegraphics[width=0.25\textwidth]{eratos}
  \end{figure}
\end{frame}

\begin{frame}{Introduction}
  The paper has three main points:
  \begin{itemize}
  \item Why a widespread implementation is \imp{\emph{not}} the
    \sieve{}.
  \item How and algorithm that \imp{\emph{is}} the \sieve{} can be written
    in a lazy and functional way.
  \item How the choice of data structures matters.
  \end{itemize}
\end{frame}


\section{What the Sieve is and is not}

\subsection{Classic \sieve}
\begin{frame}{Classic imperative Sieve}
  Classic and imperative \sieve.

  \begin{enumerate}
  \item Start with a table of naturals $2, 3, \dots, n$. Set $p=2$.
  \item Declare $p$ to be prime, and cross off all the multiples of that number in the
    table, starting from $p^2$.
  \item Find the next number in the table after $p$ that is not yet crossed off and set
    $p$ to that number; and then repeat from step 2 until $\sqrt{n}$ is
    reached.
  \item All the uncrossed naturals are primes.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Classic imperative sieve}
  \begin{figure}[h]
    \centering
      \includegraphics[width=0.85\textwidth]{sieve}
  \end{figure}
  \href{./figures/animation.gif}{\textcolor{orange}{Animated example}}
\end{frame}

\subsection{Unfaithful \sieve{}}
\begin{frame}[fragile]{Unfaithful sieve}
  Elegant but highly \imp{inefficient} implementation.
  \begin{itemize}
  \item Computing the \nth{4000} prime takes 4.7 seconds.
  \item Worse than trial division.
  \item This is \imp{\emph{not}} the \sieve{}. Often confused with the
    genuine sieve.
  \end{itemize}
\inputminted[
frame=lines
, framesep=2mm
, baselinestretch=1.2
, fontsize=\footnotesize
, linenos
]{haskell}{code/unfaithful.hs}
\end{frame}

\subsection{Comparison}
\begin{frame}
  \frametitle{Comparison}
  Why is the unfaithful \sieve{} inefficient? \pause{}\\~\\
  Imagine that we are finding all the primes up to 541 and we have
  just asserted that 17 is prime.
  \begin{itemize}
  \item The \textbf{classic} algorithm performs \imp{15} crossings: $17^2 = 289, 306, 323,
    \dots , 510, 527$.
  \item The \textbf{unfaithful} sieve performs \imp{99} tests: $19,23,29, \dots, 523,
    527$. It checks all the numbers that are not multiple of
    $2, 3, 5, 7, 11 \text{ or } 13$ (i.e. the smaller primes).
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Asymptotic comparison}
  \centering
  \begin{tabular}{ l || c | c | c }
    \toprule
    Algorithm & Genuine Sieve & Trial Division & Unfaithful Sieve \\
    Cost & $\Theta(n\log \log n)$
                        & $\Theta (\frac{n\sqrt{n}}{2(\log n)^2})$
                                     & $ \Theta (\frac{n^2}{2{(\log n)}^2})$\\
    \bottomrule
  \end{tabular}
\pause{}
  \begin{figure}[h]
    \centering
    \includegraphics[trim={5cm 9cm 6cm 7cm},clip, width=0.95\textwidth]{comp1}
  \end{figure}
\end{frame}

\subsection{Conclusions}
\begin{frame}[fragile]{First conclusion}
  Although pretty, this is \imp{\emph{NOT}} the \sieve{}!
\inputminted[
frame=lines
, framesep=2mm
, baselinestretch=1.2
, fontsize=\footnotesize
, linenos
]{haskell}{code/unfaithful.hs}
\end{frame}


\section{Incremental functional \sieve{}}

\begin{frame}
  \frametitle{Functional Sieve}
  Can the genuine \sieve{} be implemented \imp{efficiently}
  and elegantly in a purely \imp{functional} language and produce an \imp{infinite}
  list? \pause{} \\

  \begin{center}
    \huge \textcolor{Green4}{Yes!}\pause{} How?
  \end{center}
\end{frame}

\subsection{How does it work?}
\begin{frame}
  \frametitle{Functional Sieve}
  \textcolor{Red3}{Problem}: Since we have an infinite list, we cannot
  cross off all multiples of a prime $p$ in a row. \\ \pause{}
  \textcolor{Green4}{Solution}: For each prime $p$ discovered so far we keep the infinite list
  of multiples $[2p, 3p, \dots]$ in a table. The first element of the
  list is the key.
  \pause{}
  \\~
  For example,
  \begin{enumerate}
  \item We just discovered that 19 is prime.
  \item What is the next composite to cross?
    \\ Look up minimum key $\rightarrow$ $(20, [[20, 22,
    \dots],[20,25, \dots]])$
  \item Delete position 20. Then add $[22, 24,\dots]$ to position 22
    and $[25, 30, \dots]$ to position 25.
  \end{enumerate}
  \pause{} What do we need?
  \begin{itemize}
  \item Look up minimum key.
  \item Delete minimum key.
  \item Insert any key.
  \end{itemize}

\end{frame}

\subsection{Data structure}
\begin{frame}
  \frametitle{Data structure choice}
    Which data structure fits our needs? \pause{}
    \begin{itemize}
    \item TreeMap.
      \begin{tabular}{ l || c | c | c }
        \toprule
        Operation & Look up min key & Delete min key & Insert \\
        Cost & $\Theta(\log n)$ & $\Theta(\log n)$ & $ \Theta(\log n)$\\
        \bottomrule
      \end{tabular}
      \pause{} \\~\\
    \item Fibonacci heap.
      \begin{tabular}{ l || c | c | c }
        \toprule
        Operation & Look up min key & Delete min key & Insert \\
        Cost & \cellcolor{Green1!50} $\Theta(1)$ & $\Theta(\log n)$ &
                                                                      $ \cellcolor{Green1!50} \Theta(1)$\\
        \bottomrule
      \end{tabular}
\end{itemize}
~\\
Heaps are the answer!
\end{frame}

\begin{frame}
  \frametitle{Comparison}
  \begin{figure}[h]
    \centering
    \includegraphics[trim={5cm 8cm 35cm 12cm},clip, width=0.75\textwidth]{comp2}
  \end{figure}
\end{frame}

\subsection{Further improvements}
\begin{frame}[fragile]
  \frametitle{Further improvements}
  How can we further improve the algorithm?
  \begin{itemize}
  \item \imp{Ignore even numbers} from the beginning.
    \begin{minted}[
    frame=lines
    , framesep=2mm
    , baselinestretch=1.2
    , fontsize=\footnotesize
    ]{haskell}
-- Old:
-- primes = sieve [2..]
primes = [2] ++ sieve [3,5..]
\end{minted}
    For each prime $p$ we keep $[3p, 5p,
    \dots]$ instead of $[2p, 3p, \dots]$.
    \pause{}
  \item Can we do the same for multiples of 3? and 5? and$\ldots$?
    \pause{} \\
    Yes! Using a \imp{wheel}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Further improvements}
  \framesubtitle{wheel}
  A wheel is a \imp{cyclic} infinite sequence of numbers. \\
  \imp{Spinning} a wheel means repeatedly adding the numbers in the wheel to
  a starting number.
  \begin{itemize}
  \item The naïve wheel is $1,1,\dots$. \\
    Spinning starts at 2.
  \item The wheel ignoring the even numbers is $2,2,\dots$. \\
    Spinning starts at 3.
  \item The wheel ignoring multiples of 2 and 3 is $cycle([2,4]) =
    2,4,2,4, \dots$. \\
    Spinning starts at 5.
  \item The wheel ignoring multiples of 2, 3 and 5 is
    $cycle([4,2,4,2,4,6,2,6])$. \\
    Spinning starts at 7.
  \item $\dots$
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Further improvements}
  \begin{itemize}
  \item By ignoring the multiples of 2, 3, 5 and 7 we eliminate \imp{$77\%$}
of the work.
\item Using a larger wheel does not improve the performance of the algorithm.
  \end{itemize}

\begin{minted}[
    frame=lines
    , framesep=2mm
    , baselinestretch=1.2
    , fontsize=\footnotesize
    ]{haskell}
-- definitive version
[2, 3, 5, 7] ++ sieve (spin wheel2357 11)
\end{minted}
\end{frame}

\subsection{Comparison}
\begin{frame}
  \frametitle{Comparison}
  \begin{figure}[h]
    \centering
    \includegraphics[trim={32cm 8cm 6cm 12cm},clip,
    width=0.75\textwidth]{comp2}
  \end{figure}
\end{frame}

\section{Conclusions}
\begin{frame}
  \frametitle{Conclusions}
  \begin{itemize}
  \item Don't be confused by the siren chant of the unfaithful \sieve{}.
  \item An efficient and elegant implementation of the genuine
    \sieve{} is possible and relatively simple.
  \item This is possible thanks to laziness. Laziness is an ally, not
    an enemy.
  \item There exist more efficient (although more complicated)
    alternatives to the \sieve{}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Questions}
   \begin{center}
    \Huge Questions
  \end{center}
\end{frame}

\end{document}
